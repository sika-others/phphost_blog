---
layout: post
title: PHP Host 2.0 beta je venku
---

Od roku 2012 kdy jsem spustil PHP Host (tehdy jeste php.h4y.cz) ubehlo hodne casu, ale az ted prisla prvni velka zmena. Do ted se system moc nerozvyjel a zustal ve dost nedodelane fazi. Kdyz jsem chtel letos navazat na vyvoj a dokoncit nejake veci, zjistil jsem ze kod, ktery jsem psal v roce 2012 je tak strasny, ze jsem to radeji napsal znovu.

Novy PHP Host bezi zatim na domene <http://beta.phphost.cz> a jeste asi nejakou dobu pobezi, nez do noveho systemu premigruji soucasne hostingy. Novy system je psan s ohledem ze portal (Django app) muze bezet na jinem serveru nez hostovane weby. To zatim neplatilo a vyplivalo z toho hodne problemu a omezeni. Z principu bylo mozne pouzivat jen jeden server pro hostovane weby coz ted odpada, dale odpada problem spousteni Django app na serveru s hostingy. Hostingovych serveru muze byt vice. Vyhodou je ze novy system hostingu muze bezet zaroven se starym na stejnem serveru, proto nebude migrace tak slozita.

Administrace pocita s MySQL i PostgreSQL databazemi a da se z administrace databaze i vytvorit.

Dale jsem zlepsil podporu, mame ticketovaci system a muzete jim napsat z jakymkovim dotazem na <support@phphost.cz>.

Platby za hosting prijimame zatim jen na ucet (fio) a Bitcoiny, zpracovane prez [MyBitcoinPayments](http://mybitcoinpayments.com). Platby Bitcoinem maji 10% slevu.

Podpora HTTPS je, zatim ale nepresmerovavame vsechen trafic, protoze mame pouze certifikaty podepsane vlastni certifikacni autoritou, kterou si muzete stahnout a pridat do prohlizece [PHP Host CA](https://ondrejsika.com/static/content/ca/ca_phphost.pem), pgp signature a dalsi informace na <https://ondrejsika.com/ca.html>.

Mame naplanovane dalsi skvele funkce, sledujte nas [blog](http://blog.phphost.cz) a twitter [@phphostcz](https://twitter.com/phphostcz).

Pro pristup do bety piste na <support@phphost.cz>.

