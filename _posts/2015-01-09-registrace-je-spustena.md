---
layout: post
title: Registrace je spustena
---

Od dneska je spustena moznost registrace na [beta.phphost.cz](http://beta.phphost.cz). Registrace je zatim provizorni, ale pro testovaci ucely postacuje.

Odkaz primo na registraci je <http://beta.phphost.cz/registration/>

Provoz v testovacim obdobi je zdarma, vsechny weby co si spustite Vam po zkonceni testovaciho obdobi nechame bezet dal, ale nezarucujeme, ze se nemuze neco pokazit, proto zatim zdarma.

Pokud nechcete na nas server zatim smerovat vlastni domenu, muzete pouzit nejakou nasi. CNAME zaznam __\*.php.h4y.cz__ a __\*.p.h4y.cz__ ukazuji na tento server, proto si na nem muzete vybrat nejaky prefix a vyzkouset hosting takto. Rad bych vsak kvuli zachovani jmeneho prostoru vyuzivali tyto domeny s ohledem na ostatni ve tvaru __sitename-username.p.h4y.cz__.

Pokud budete mit jakykoliv dotaz, pripominku nebo napad na vylepseni, kontaktujte nasi podporu na <support@phphost.cz>
